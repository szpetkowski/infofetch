import json, os

from django.conf import settings
from django.shortcuts import render

def news_home_view(request):
    feed_file_path = os.path.join(settings.BASE_DIR, 'news/data/feed.json')
    with open(feed_file_path) as feed_file:
        feed_data = json.load(feed_file)

    feed_data.sort(key=lambda x: x['score'], reverse=True)

    return render(request, 'news_home.html', {'data': feed_data})

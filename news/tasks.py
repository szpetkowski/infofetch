import json, os, requests

from django.conf import settings

from celery import shared_task

@shared_task
def news_fetch_task():
    reddit_subs = [
        'linux',
        'linux_gaming',
        'programming',
        'rust',
        'technology',
        'web_design',
        'gamedev',
    ]

    headers = {
        'User-Agent': 'infofetch bot',
    }

    hot_pattern = 'https://www.reddit.com/r/{}/hot.json'

    feed_data = []
    for sub in reddit_subs:
        r = requests.get(hot_pattern.format(sub), headers=headers)
        try:
            json_entries = r.json()['data']['children']
        except:
            json_entries = r.json()[0]['data']['children']

        feed_data.append([])
        for child in json_entries:
            if sub == 'linux':
                icon = 'fa-linux'
            elif sub == 'linux_gaming':
                icon = 'fa-gamepad'
            elif sub == 'programming':
                icon = 'fa-code'
            elif sub == 'rust':
                icon = 'fa-fire'
            elif sub == 'technology':
                icon = 'fa-laptop'
            elif sub == 'web_design':
                icon = 'fa-magic'
            elif sub == 'gamedev':
                icon = 'fa-wrench'
            else:
                icon = 'fa-bookmark'

            feed_data[-1].append({
                'title': child['data']['title'],
                'text': child['data']['selftext'],
                'url': child['data']['url'],
                'score': child['data']['score'],
                'comms': child['data']['num_comments'],
                'comms_url': 'https://reddit.com' + child['data']['permalink'],
                'icon': icon,
                'source': '/r/' + sub,
            })

    feed_data = [x for y in zip(*feed_data) for x in y]

    feed_dir_path = os.path.join(settings.BASE_DIR, 'news/data')
    if not os.path.isdir(feed_dir_path):
        os.mkdir(feed_dir_path)

    feed_file_path = os.path.join(settings.BASE_DIR, 'news/data/feed.json')
    with open(feed_file_path, 'w') as feed_file:
        json.dump(feed_data, feed_file)

    return True
